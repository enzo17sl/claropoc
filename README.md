# CLARO POC #

POC para CLARO de Enzo Oscar Sanchez.
### Contenido del proyecto ###

* Repositorio GIT con el proyecto realizado en IONIC ANGULAR.
* Utilizacion de POSTMAN para realizar prueba de servicios.
* [Tablero en TRELLO con las tareas indicadas](https://trello.com/b/QbpNSgCo/poc)

### Etapas del proyecto ###

* RELEASE 1: Diseño visual y funcion basica (loguear y poder cambiar la contraseña). 
* RELEASE 2: Validar si hay un cambio de password pendiente al momento de haberse cortado la conexion a internet.
* RELEASE 3: Agregar sistema de notificaciones push para solicitar cambio de password.