import { ImageCustomModule } from './../../shared/image-custom/image-custom.module';
import { ToolbarModule } from './../../shared/toolbar/toolbar.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ToolbarModule,
    ImageCustomModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
