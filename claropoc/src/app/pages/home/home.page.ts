import { BasePage } from './../../shared/base-page/base-page';
import { Component } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage extends BasePage {

  constructor(navController: NavController, menuController: MenuController, network: Network) {
    super(network, navController,menuController);
    this.onEnableIonMenu();
  }

}
