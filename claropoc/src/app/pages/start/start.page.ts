import { BasePage } from './../../shared/base-page/base-page';
import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, AlertController, ToastController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage extends BasePage implements OnInit {

  constructor(
    network: Network,
    navControler: NavController,
    menuController: MenuController,
    private messagingService: MessagingService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) {
    super(network,navControler,menuController);
    this.listenForMessages();
  }

  ngOnInit() {
    
  }
  /* Metodo temporal para seguir el flow de renovacion.*/
  onShowRenewPassword() {
     this.onNavigateToFoward('login/renew');
    //this.deleteToken();
    //this.requestPermission();
  }
  listenForMessages() {
    this.messagingService.getMessages().subscribe(async (msg: any) => {
      const alert = await this.alertCtrl.create({
        header: msg.notification.title,
   
   
        subHeader: msg.notification.body,
        message: msg.data.info,
        buttons: ['OK'],
      });
 
      await alert.present();
    });
  }
 
  requestPermission() {
    this.messagingService.requestPermission().subscribe(
      async token => {
        const toast = await this.toastCtrl.create({
          message: 'Got your token',
          duration: 2000
        });
        toast.present();
      },
      async (err) => {
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: err,
          buttons: ['OK'],
        });
 
        await alert.present();
      }
    );
  }
 
  async deleteToken() {
    this.messagingService.deleteToken();
    const toast = await this.toastCtrl.create({
      message: 'Token removed',
      duration: 2000
    });
    toast.present();
  }
}
