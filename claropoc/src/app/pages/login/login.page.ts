import { LocalStorageService } from 'src/app/shared/providers/localstorage.service';
import { BasePage } from './../../shared/base-page/base-page';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  passwordForm: FormGroup;
  BUTTON_DESCRIPTON = 'ACCEDER';
  buttonTitle = this.BUTTON_DESCRIPTON;
  hiddenSpinner = true;
  constructor(
    private localStorageService: LocalStorageService,
    navController: NavController,
    menuController: MenuController,
    network: Network) {
    super(network, navController, menuController);
  }

  ngOnInit() {
    this.passwordForm = new FormGroup({
      userId: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', [Validators.required, Validators.minLength(2)])
    });
  }
  onSubmit() {
    this.onShowSpinner();
    this.localStorageService.getCurrentUser().then(res => {
      if (res.userId === this.passwordForm.get('userId').value && res.author === this.passwordForm.get('password').value) {
        this.onHideSpinner();
        this.presentToast('credenciales correctas');
        this.onNavigateToRoot('home');
      } else {
        this.onHideSpinner();
        this.presentToast('credenciales incorrectas');
      }
    });
  }

  onShowSpinner() {
    this.hiddenSpinner = false;
    this.buttonTitle = '';
  }
  onHideSpinner() {
    this.hiddenSpinner = true;
    this.buttonTitle = this.BUTTON_DESCRIPTON;
  }
}
