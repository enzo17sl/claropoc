import { LocalStorageService } from 'src/app/shared/providers/localstorage.service';
import { NavController, MenuController } from '@ionic/angular';
import { BasePage } from './../../../shared/base-page/base-page';
import { UsersService } from './../../../modules/providers/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/interfaces/user';
import { Network } from '@ionic-native/network/ngx';
@Component({
  selector: 'app-renew',
  templateUrl: './renew.page.html',
  styleUrls: ['./renew.page.scss'],
})
export class RenewPage extends BasePage implements OnInit {

  passwordForm: FormGroup;
  BUTTON_DESCRIPTON = 'ENVIAR';
  buttonTitle = this.BUTTON_DESCRIPTON;
  hiddenSpinner = true;
  constructor(
    private localStorageService: LocalStorageService,
    private usersService: UsersService,
    network: Network,
    navController: NavController,
    menuController: MenuController) {
    super(network,navController, menuController);
  }

  ngOnInit() {
    this.passwordForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(2)]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(2)]),
      repeatPassword: new FormControl('', [Validators.required, Validators.minLength(2)]),
    });
  }

  onSubmit(): void {
    if (this.passwordMatchValidator(this.passwordForm)) {
      this.onValidatePassword();
    } else {
      this.presentToast('La contraseña ingresa no coincide');
    }
  }

  passwordMatchValidator(g: FormGroup) {
    return g.get('newPassword').value === g.get('repeatPassword').value ? true : false;
  }

  onValidatePassword() {
    this.onShowSpinner();
    this.localStorageService.getCurrentUser().then(res => {
      if (res.author === this.passwordForm.get('password').value) {
        this.onChangePassword(res);
      } else {
        this.onHideSpinner();
        this.presentToast('La contraseña actual ingresada es incorrecta');
      }
    });
  }
  onShowSpinner() {
    this.hiddenSpinner = false;
    this.buttonTitle = '';
  }
  onHideSpinner() {
    this.hiddenSpinner = true;
    this.buttonTitle = this.BUTTON_DESCRIPTON;
  }
  onChangePassword(currentUser: User) {
    currentUser.author = this.passwordForm.get('repeatPassword').value;
    if(this.networkStatus){  
      this.usersService.updateCurrentUser(currentUser).subscribe(res => {
        this.localStorageService.setCurrentUser(res);
        this.presentToast('cambio de contraseña correcto');
        this.onHideSpinner();
        this.onNavigateToRoot('home');
      });
    }else{
      this.localStorageService.setOfflineRenewPassword(currentUser.author);
      this.presentToast('Ingresaras en modo offline, una vez que se retome la conexion se aplicara tu cambio de clave, gracias!');
      this.onHideSpinner();
      this.onNavigateToRoot('home');
    }
  }
}
