import { LoginModule } from './../../../components/login/login.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenewPageRoutingModule } from './renew-routing.module';

import { RenewPage } from './renew.page';
import { ImageCustomModule } from 'src/app/shared/image-custom/image-custom.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenewPageRoutingModule,
    LoginModule,
    ReactiveFormsModule,
    ImageCustomModule
  ],
  declarations: [RenewPage]
})
export class RenewPageModule {}
