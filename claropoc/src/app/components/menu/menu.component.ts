import { UsersService } from 'src/app/modules/providers/authentication.service';
import { LocalStorageService } from 'src/app/shared/providers/localstorage.service';
import { Network } from '@ionic-native/network/ngx';
import { BasePage } from './../../shared/base-page/base-page';
import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent extends BasePage implements OnInit {
  navigateDinamic: any;
  navigateStatic: any
  myNetwork;
  constructor(
    private usersService: UsersService,
    private localStorageService: LocalStorageService,
    navController: NavController,
    menuController: MenuController,
    network: Network) {
    super(network,navController, menuController);
    this.sideMenu();
    this.myNetwork = network
  }
  ngOnInit() {
    this.onValidatePendingActions();
  }
  sideMenu() {
    this.navigateDinamic =
      [
        {
          title: "Home",
          url: "/home",
          icon: "home"
        },
        {
          title: "Avisos",
          url: "/avisos",
          icon: "notifications"
        },
        {
          title: "Guardias",
          url: "/guardias",
          icon: "calendar"
        },
        {
          title: "Alarmas",
          url: "/alarmas",
          icon: "alarm"
        }
      ];
    this.navigateStatic = [
      {
        title: "SES",
        url: "/ses",
        icon: "calendar"
      }, {
        title: "Configuración",
        url: "/configuracion",
        icon: "settings"
      },
      {
        title: "Cerrar sesión",
        url: "/logout",
        icon: "log-out"
      },
    ];
  }

  onLogOut(){
    this.presentToast('Cerrando la sesion actual');
    this.onDisabledIonMenu();
    this.onNavigateToRoot('login');
  }
  onValidatePendingActions(){
    this.myNetwork.onChange().subscribe(res => {
      this.localStorageService.getOfflineRenewPassword().then(res => {
        if(res != null){
          this.onUpdatePassword(res);
        }
      });
    });
  }
  onUpdatePassword(newPassword: string){
    this.presentToast('Se recupero la conexion, aguarda unos segundos! estamos actualizando tu clave...');
    this.localStorageService.getCurrentUser().then(user =>{
      user.author = newPassword;
      this.usersService.updateCurrentUser(user).subscribe(updateUserRequest => {
        this.localStorageService.setCurrentUser(updateUserRequest);
        this.localStorageService.setOfflineRenewPassword(null);
        this.presentToast('Cambiamos tu contraseña con exito!');
      })
    })
  }
}