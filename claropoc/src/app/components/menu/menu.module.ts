import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
@NgModule({
    declarations: [
        MenuComponent,
        HeaderComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
      RouterModule
    ],
    exports: [
        MenuComponent
    ],
    providers: [
    ]
  })
  export class MenuModule { }
