import { LocalStorageService } from 'src/app/shared/providers/localstorage.service';
import { UsersService } from './../../../modules/providers/authentication.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  constructor(private localStorageService: LocalStorageService) { }
  currentUser: User
  ngOnInit() {
    this.localStorageService.getCurrentUser().then(res => { this.currentUser = res;});
  }

}
