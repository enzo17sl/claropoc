import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-login',
  templateUrl: './btn-login.component.html',
  styleUrls: ['./btn-login.component.scss'],
})
export class BtnLoginComponent implements OnInit {

  @Input() validate: boolean;
  @Input() hiddenSpinner: boolean;
  @Input() buttonTitle: string;
  constructor() { }

  ngOnInit() {}

}
