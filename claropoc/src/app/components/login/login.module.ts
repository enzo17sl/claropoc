import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { BtnLoginComponent } from './btn-login/btn-login.component';
import { ShowHidePasswordComponent } from './show-hide-password/show-hide-password.component';

@NgModule({
    declarations: [
        ShowHidePasswordComponent,
        BtnLoginComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
        ShowHidePasswordComponent,
        BtnLoginComponent
    ],
    providers: [
    ]
  })
  export class LoginModule { }
