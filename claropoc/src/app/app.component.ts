import { LocalStorageService } from 'src/app/shared/providers/localstorage.service';
import { UsersService } from './modules/providers/authentication.service';
import { Platform } from '@ionic/angular';
import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  initComponents = false;
  constructor(public plt: Platform, private usersService: UsersService, private localStorage: LocalStorageService) {
    this.initApp();
  }
  initApp(){
    this.plt.ready().then((readySource) => {
      console.log('Platform ready from', readySource);
      // Platform now ready, execute any required native code
      this.checkCurrentUser();
    });
  }
  checkCurrentUser(){
    this.localStorage.getCurrentUser().then(res => {
      if(res != null){
        this.initComponents = true;
      }else{
        this.usersService.getCurrentUser().subscribe(res => {
          if (res != null && res.title != "") {
            var index = res.title.indexOf(" ");
            var firstName = res.title.substr(0, index);
            var lastName = res.title.substr(index + 1);
            res.initials = firstName.substr(0,1) + lastName.substr(0,1);
            this.localStorage.setCurrentUser(res);
            this.initComponents = true;
          }
        });
      }
    });
  }
}
