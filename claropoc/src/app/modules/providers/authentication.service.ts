import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/interfaces/user';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class UsersService {
    constructor(private http: HttpClient) { }

    getCurrentUser() {
        return this.http.get<User>(`${environment.apiServiceURL}`);
    }

    updateCurrentUser(user: User) {
        return this.http.put<User>(`${environment.apiServiceURL}`, user);
    }
}