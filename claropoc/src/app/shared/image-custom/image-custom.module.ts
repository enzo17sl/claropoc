import { ImageCustomComponent } from './image-custom.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
@NgModule({
    declarations: [
        ImageCustomComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
        ImageCustomComponent
    ],
    providers: [
    ]
  })
  export class ImageCustomModule { }
