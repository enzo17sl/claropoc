import { Injectable } from "@angular/core";
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from "src/app/interfaces/user";

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    CURRENT_USER = 'currentUser';
    OFFLINE_RENEW_PASSWORD = 'renewPassword';
    constructor(private nativeStorage: NativeStorage) { }

setCurrentUser(user: User){
    this.nativeStorage.setItem(this.CURRENT_USER, {title: user.title, userId: user.userId, author: user.author, id: user.id, initials: user.initials})
  .then(
    () => console.log('Stored item!'),
    error => console.error('Error storing item', error)
  );
}
  async getCurrentUser(): Promise<User> {
    return await new Promise((resolve) => {
        try {
            this.nativeStorage.getItem(this.CURRENT_USER)
                .then(data => {
                    resolve(data);
                })
                .catch(err => {
                    resolve(null);
                });
        } catch (err) {
          resolve(null);
        }
    });
}

setOfflineRenewPassword(renewalPassword: string){
    this.nativeStorage.setItem(this.OFFLINE_RENEW_PASSWORD, renewalPassword)
    .then(
      () => console.log('Stored item!'),
      error => console.error('Error storing item', error)
    );
}
async getOfflineRenewPassword(): Promise<string> {
  return await new Promise((resolve) => {
      try {
          this.nativeStorage.getItem(this.OFFLINE_RENEW_PASSWORD)
              .then(data => {
                  resolve(data);
              })
              .catch(err => {
                  resolve(null);
              });
      } catch (err) {
        resolve(null);
      }
  });
}
}