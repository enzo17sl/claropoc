import { Network } from "@ionic-native/network/ngx";
import { MenuController, NavController, ToastController } from "@ionic/angular";

export class BasePage {
  toastController: ToastController;
  networkStatus;
  constructor(
    private network: Network,
    private navController: NavController,
    private menuController: MenuController) {
    this.menuController.enable(false, 'firstMenu');
    this.networkStatus = this.network.type === 'none' ? false : true; 
    this.getNetworkStatus();
  }

  async presentToast(errorDescription) {
    this.toastController = new ToastController();
    const toast = await this.toastController.create({
      message: errorDescription,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  async onNavigateToFoward(url: string) {
    this.navController.navigateForward(url);
  }
  async onNavigateToRoot(url: string) {
    this.navController.navigateRoot(url);
  }
  onEnableIonMenu() {
    this.menuController.enable(true, 'firstMenu');
  }
  onDisabledIonMenu() {
    this.menuController.enable(false, 'firstMenu');
  }

  getNetworkStatus(){
    this.network.onChange().subscribe(res => {
      console.log('Cambio network a: ' + res);
      if(res === 'connected'){
        this.networkStatus = true;
      }else{
        this.networkStatus = false;
      }
    });
  }
}