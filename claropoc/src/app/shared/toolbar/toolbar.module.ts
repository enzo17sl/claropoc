import { ToolbarMenuComponent } from './toolbar-menu/toolbar-menu.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
@NgModule({
    declarations: [
        ToolbarMenuComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
        ToolbarMenuComponent
    ],
    providers: [
    ]
  })
  export class ToolbarModule { }
