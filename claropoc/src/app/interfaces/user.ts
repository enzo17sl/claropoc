export interface User{
    title: string;
    userId: string;
    author: string;
    id: string;
    initials?: string;
}