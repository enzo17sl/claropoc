// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiServiceURL: 'https://nico.emicba.me/posts/9391',
  firebase: {
    apiKey: "AIzaSyAbCu_iXE2Ki76uQqUOMtOrXeSUOGtImpI",
    authDomain: "claro-push-enzo.firebaseapp.com",
    projectId: "claro-push-enzo",
    storageBucket: "claro-push-enzo.appspot.com",
    messagingSenderId: "205224827057",
    appId: "1:205224827057:web:81f903c5f865e5f3a22a90",
    measurementId: "G-9SEYMKX7LM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
